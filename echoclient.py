from twisted.internet import protocol, reactor

global realflag
address = "127.0.0.1"
port = 8080
realflag = True

class EchoClient (protocol.Protocol):


    def connectionMade(self):
        print 'Successfully connected to the server.'
        self.transport.write('hello!')

    def dataReceived(self,data):
        global realflag
        print 'recevied from server: ' + data

        if realflag:
            input1 = raw_input('send message, choose "go", close the connection choose "exit": ')
            if input1 == 'go':
                message = raw_input('Your message is : ')
                self.transport.write(message)
            else:
                realflag=False
        # self.transport.write(data)
        # print data

p = EchoClient()

class EchoFactory(protocol.ClientFactory):
    def startedConnecting(self, connector):
        print('startd to connect.')

    def buildProtocol(self, addr):
        print 'Successfully connect to the server'
        return p

    def clientConnectionFailed(self, connector, reason):
        reactor.stop()
        print ('Connection Failed!')

    def clientConnectionLost(self, connector, reason):
        reactor.stop()
        print ('Connection Closed')

reactor.connectTCP(address, port, EchoFactory())
reactor.run()
