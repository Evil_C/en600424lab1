from twisted.internet import protocol, reactor

class Echoserver(protocol.Protocol):
    def dataReceived(self, data):
        host = self.transport.getPeer().host
        port = self.transport.getPeer().port
        print 'receving data: ' +  '< '+data+ ' > from '+ host + ':'+ str(port)
        self.transport.write(data)

class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Echoserver()

reactor.listenTCP(8080, EchoFactory())
reactor.run()
