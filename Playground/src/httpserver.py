from twisted.internet import protocol, reactor
import re
from playground.twisted.endpoints import GateServerEndpoint

global client_address
#client_address="client"
dicttable = {}

root = './'
class HTTPserver(protocol.Protocol):
    def connectionMade(self):
        global client_address
        address = self.transport.getPeer()[0]
        port = self.transport.getPeer()[1]
        client_address = address + ':' + str(port)
        print 'Client ' + client_address + ' Connected!'
        self.makeLog(client_address,'CONNECT','')
        dicttable[client_address]=0

    def requestReceive(self,data):
        global client_address
	pattern = re.findall('GET (.*?) HTTP', data)
        document = root + pattern[0]
	try:
            f= open(document,'r')
            content = f.read()
            f.close()
            self.transport.write(content)
            self.makeLog(client_address,'SEND', content)
        except Exception, err:
            document = root + 'index.html'
            address = self.transport.getPeer()[0]
            port = self.transport.getPeer()[1]
            client_address = address + ':' + str(port)
            if dicttable[client_address] <3:
                f= open(document,'r')
                content = f.read()
                f.close()
                dicttable[client_address] +=1
            else:
                print err
                content = '404 not found'
            self.transport.write(content)
            self.makeLog(client_address, 'SEND', content)
        return content

    def dataReceived(self, data):
        global client_address
        self.makeLog(client_address, "RECEIVE", data)
        self.requestReceive(data)

    def makeLog(self, dest, type, data):
        dest_addr = dest
        if type == 'CONNECT':
                log = '[CONNECT] connect to '+ dest_addr + '\n'
        elif type == 'SEND':
                log = '[SEND] send to ' + dest_addr + ' ' + data + '\n'
        elif type == 'RECEIVE':
                log = '[RECEIVE] receive from ' + dest_addr + ' ' + data + '\n'
        else:
                log = 'Wrong type of message' + '\n'

        f= open ('HTTPserver.log', 'a')
        f.write(log)
        f.close()

class HTTPFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return HTTPserver()

endpoint = GateServerEndpoint(reactor,9123,"127.0.0.1",2901)
endpoint.listen(HTTPFactory())
#reactor.listenTCP(9123, HTTPFactory())
reactor.run()
