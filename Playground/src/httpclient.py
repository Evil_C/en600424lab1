from twisted.internet import protocol, reactor
from playground.twisted.endpoints import GateClientEndpoint
address = "127.0.0.1"
port = 9123
flag = True

class HTTPClient(protocol.Protocol):
    def connectionMade(self):
        self.HTTPrequest()

    def HTTPrequest(self):
        count=0
        document = raw_input('Please input the document you want get: ')
        data = 'GET '+ document+' HTTP/1.1\r\nHost:127.0.0.1\r\nConnection: close\r\n'
        self.transport.write(data)

    def dataReceived(self, data):
        print 'received from server'
        print data
        if flag == True:
            self.connectionMade()

p = HTTPClient()

class HTTPFactory(protocol.ClientFactory):

    def startedConnecting(self, connector):
        print ('Start to connect.')

    def buildProtocol(self, addr):
        print ('Successfully connect to the server')
        return p

    def clientConnectionFailed(self, connector, reason):
        reactor.stop()
        print ('Connection Failed!!')

    def clientConnectionLost(self, connector, reason):
        reactor.stop()
        print ('Conneection Lost!!')

endpoint = GateClientEndpoint(reactor,"20164.0.0.1",9123,"127.0.0.1",2901)
endpoint.connect(HTTPFactory())
#reactor.connectTCP(address, port,HTTPFactory())
reactor.run()
