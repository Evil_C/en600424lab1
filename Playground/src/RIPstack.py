#!/usr/bin/python
# written by evilC

from twisted.internet.protocol import Protocol, Factory
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
from playground.network.message.StandardMessageSpecifiers import UINT4, OPTIONAL, STRING, DEFAULT_VALUE, LIST, BOOL1
import random
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from playground.crypto import X509Certificate


class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIP.RIPMessageID"
    MESSAGE_VERSION = "1.0"

    BODY = [
        ("sequence_number", UINT4, OPTIONAL),
        ("acknowledgement_number", UINT4, OPTIONAL),
        ("signature", STRING, DEFAULT_VALUE("")),
        ("certificate", LIST(STRING), OPTIONAL),
        ("sessionID", STRING, OPTIONAL),
        ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
        ("close_flag", BOOL1, DEFAULT_VALUE(False)),
        ("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
        ("reset_flag", BOOL1, DEFAULT_VALUE(False)),
        ("data", STRING, DEFAULT_VALUE("")),
        ("OPTIONS", LIST(STRING), OPTIONAL)
    ]


class RipTransport(StackingTransport):
    def __init__(self, lowerTransport):
        StackingTransport.__init__(self, lowerTransport)

    def write(self, data):
        ripMessage = RipMessage()
        ripMessage.data = data
        self.lowerTransport().write(ripMessage.__serialize__())

    def write_ripMessage(self, ripMessage):
        self.lowerTransport().write(ripMessage.__serialize__())

class RipCrypto():
    def __init__(self, path):
        self.RSA = self.importSk(path)

    def importSk(self, path):
        with open(path) as f:
            Key = f.read()
        RSAKey = RSA.importKey(Key)
        RSASigner = PKCS1_v1_5.new(RSAKey)
        return RSASigner

    def sign(self, data):
        HASH_tmp = SHA256.new()
        HASH_tmp.update(data)
        signatureBytes = self.RSA.sign(HASH_tmp)
        return signatureBytes

    def loadCert(self, certBytes):
        CERTS = X509Certificate.loadPEM(certBytes)
        return CERTS

    def verifySignature(self, cert, data, signature):
        cert = self.loadCert(cert)
        PublicKey = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(PublicKey)
        RSA_Verifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(data)
        result = RSA_Verifier.verify(hasher, signature)
        return result

    def verifyCertChain(self, rootCertBytes, user_certBytes):
        cert = self.loadCert(user_certBytes)
        rootCert = self.loadCert(rootCertBytes)

        rootPk = RSA.importKey(rootCert.getPublicKeyBlob())
        rootVerifier = PKCS1_v1_5.new(rootPk)

        bytesToVerify = cert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rootVerifier.verify(hasher, cert.getSignatureBlob()):
            return False
        return True

class RipClientProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):

        self.__buffer = ""
        self.handshake_status=1 # 1 for 1st hand shake, 2 -> 2 3->3

        self.my_sk_path = "/home/evil-c/NS/firstsetp/NSevilc_0_0"
        self.my_cert_path = "/home/evil-c/NS/firstsetp/chaolei_user_signed.cert"
        self.CA_cert_path = "/home/evil-c/NS/firstsetp/chaolei_signed.cert"
        self.__crypto = RipCrypto(self.my_sk_path)

        self.load_my_cert()
        self.load_CA_cert()
        self.load_sk()
        self.__certs = [self.__myCertBytes,self.__CACertBytes]

    def load_my_cert(self):
        with open(self.my_cert_path) as f:
            cert = f.read()
        self.__myCertBytes = cert

    def load_CA_cert(self):
        with open(self.CA_cert_path) as f:
            CACert = f.read()
        self.__CACertBytes = CACert

    def load_sk(self):
        with open(self.my_sk_path) as f:
            sk = f.read()
        self.__mySkBytes = sk

    def Write_message(self, active=False,  seq_num=0, ack_num=0, certs=[], session_id=0, ACK=False, FIN=False, SNN=False, RST=False, data="", options=[]):
        if active:
            ripMessage = RipMessage()
            ripMessage.sequence_number = seq_num
            ripMessage.acknowledgement_number = ack_num
            ripMessage.certificate = certs
            ripMessage.sessionID = session_id
            ripMessage.acknowledgement_flag = ACK
            ripMessage.close_flag = FIN
            ripMessage.sequence_number_notification_flag = SNN
            ripMessage.reset_flag = RST
            ripMessage.OPTIONS = options
            ripMessage.signature = ""
            sig_data = ripMessage.__serialize__()
            sig = self.__sign(sig_data)
            ripMessage.signature = sig
            self.__higherTransport.write_ripMessage(ripMessage)
        else:
            self.__higherTransport.write(data)
        self.__seq_num += 1

    def __verify_certs(self, ripMessage, Nonce_number):
        # ripMessage.certificate
        certs = ripMessage.certificate
        if not self.__crypto.verifyCertChain(certs[Nonce_number+1], certs[Nonce_number+0]):
            return False
        return True

    def __verify_ack_num(self, ripMessage):
        if not ripMessage.acknowledgement_flag:
            return False

        if self.__seq_num != ripMessage.acknowledgement_number:
            return False
        return True

    def __verify_SNN(self, ripMessage):
        if not ripMessage.sequence_number_notification_flag:
            return False
        return True

    def __verify_signature(self, ripMessage):
        certBytes = self.__peer_cert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.__crypto.verifySignature(certBytes, data, signature):
            return False
        return True

    def __sign(self, data):
        return self.__crypto.sign(data)

    def __verify_Nonce1(self, ripMessage):
        #signed_nonce1 = ripMessage.OPTIONS[1]
        signed_nonce1 = ripMessage.certificate[1]
        certBytes = self.__peer_cert
        if not self.__crypto.verifySignature(certBytes, hex(self.__Nonce1+1)[2:], signed_nonce1):
            return False
        return True

    def seq_num_gen(self):
        result = random.randint(0,0xff)
        return result

    def nonce_gen(self):
        result = random.randint(0,0xff)
        return result

    def send_handshake_1(self):
        self.__seq_num = self.seq_num_gen() #random.randint(0, 0xff)
        self.__Nonce1 = self.nonce_gen() #random.randint(0, 0xff)
        #options = [str(self.__Nonce1)]
        options = ""
        certs = [hex(self.__Nonce1)[2:]] + self.__certs
        self.Write_message(True, self.__seq_num, 0, certs, 0, False, False, True, False,"", options)

    def receive_handshake_2(self, ripMessage):
        Nonce_number = 2
        if not self.__verify_certs(ripMessage, Nonce_number):
            return False
        self.__peer_cert = ripMessage.certificate[Nonce_number+0]
        if not self.__verify_signature(ripMessage):
            return False
        if not self.__verify_ack_num(ripMessage):
            return False
        if not self.__verify_SNN(ripMessage):
            return False
        if not self.__verify_Nonce1(ripMessage):
            return False
        self.__ack_num = ripMessage.sequence_number + 1
        #self.__Nonce2 = ripMessage.OPTIONS[0]
        self.__Nonce2 = int(ripMessage.certificate[0], 16)
        return True

    def send_handshake_3(self):
        signed_nonce2 = self.__sign(hex(self.__Nonce2 + 1)[2:])
        #options = [signed_nonce2]
        certs = [signed_nonce2] + self.__certs
        options = ""
        self.Write_message( True, self.__seq_num, self.__ack_num, certs, 0, True, False, False, False, "", options)

    def __handshake(self, ripMessage):
        if self.handshake_status == 1:
            self.send_handshake_1()
            self.handshake_status = 2
        elif self.handshake_status == 2:
            success = self.receive_handshake_2(ripMessage)
            if not success:

                self.handshake_status = 1
                return False
            self.send_handshake_3()
            self.handshake_status = 3
            self.makeHigherConnection(self.__higherTransport)
        return True

    def connectionMade(self):

        self.__higherTransport = RipTransport(self.transport)
        self.__handshake("")

    def dataReceived(self, data):
        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(data)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            print "Client side: We had a deserialization error", e
            return
        if self.handshake_status != 3:
            success = self.__handshake(ripMessage)
            if not success:
                exit(0)
        else:
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.__buffer and self.dataReceived("")

    def SendMessage(self, data): #done

        self.__higherTransport.write(data)

class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        self.__buffer = ""
        self.handshake_status=1

        self.my_sk_path = "/home/evil-c/NS/firstsetp/NSevilc_0_0"
        self.my_cert_path = "/home/evil-c/NS/firstsetp/chaolei_user_signed.cert"
        self.CA_cert_path = "/home/evil-c/NS/firstsetp/chaolei_signed.cert"

        self.load_my_cert()
        self.load_CA_cert()
        self.load_sk()
        self.__certs = [ self.__myCertBytes,self.__CACertBytes]

        self.__crypto = RipCrypto(self.my_sk_path)


    def load_my_cert(self):
        with open(self.my_cert_path) as f:
            certBytes = f.read()
        self.__myCertBytes = certBytes


    def load_CA_cert(self):
        with open(self.CA_cert_path) as f:
            CACertBytes = f.read()
        self.__CACertBytes = CACertBytes

    def load_sk(self):
        with open(self.my_sk_path) as f:
            skBytes = f.read()
        self.__mySkBytes = skBytes

    def connectionMade(self):
        self.__higherTransport = RipTransport(self.transport)


    def Write_message(self,  active=False,  seq_num=0, ack_num=0, certs=[], session_id=0, ACK=False, FIN=False, SNN=False, RST=False,data="", options=[]):

        if active:
            ripMessage = RipMessage()
            ripMessage.sequence_number = seq_num
            ripMessage.acknowledgement_number = ack_num
            ripMessage.certificate = certs
            ripMessage.sessionID = session_id
            ripMessage.acknowledgement_flag = ACK
            ripMessage.close_flag = FIN
            ripMessage.sequence_number_notification_flag = SNN
            ripMessage.reset_flag = RST
            ripMessage.OPTIONS = options
            ripMessage.signature = ""
            sig_data = ripMessage.__serialize__()
            sig = self.__sign(sig_data)
            ripMessage.signature = sig
            self.__higherTransport.write_ripMessage(ripMessage)
        else:
            self.__higherTransport.write(data)
        self.__seq_num += 1

    def __verify_certs(self, ripMessage, Nonce_number):

        certs = ripMessage.certificate
        if not self.__crypto.verifyCertChain(certs[Nonce_number+ 1], certs[Nonce_number + 0]):
            print "server verify certs wrong"
            return False
        return True

    def __verify_signature(self, ripMessage):
        certBytes = self.__peer_cert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.__crypto.verifySignature(certBytes, data, signature):
            return False
        return True

    def __verify_ack_num(self, ripMessage):
        if not ripMessage.acknowledgement_flag:
            return False

        if self.__seq_num != ripMessage.acknowledgement_number:
            return False
        return True

    def __verify_SNN(self, ripMessage):
        if not ripMessage.sequence_number_notification_flag:
            return False
        return True

    def __verify_Nonce2(self, ripMessage):
        #signed_nonce2 = ripMessage.OPTIONS[0]
        signed_nonce2 = ripMessage.certificate[0]
        certBytes = self.__peer_cert
        if not self.__crypto.verifySignature(certBytes, hex(self.__Nonce2+1)[2:], signed_nonce2):
            return False
        return True

    def __sign(self, data):
        return self.__crypto.sign(data)

    def receive_handshake_1(self, ripMessage):
        Nonce_number = 1
        if not self.__verify_certs(ripMessage, Nonce_number):
            return False
        self.__peer_cert = ripMessage.certificate[Nonce_number+0]
        if not self.__verify_signature(ripMessage):
            print "1 verify signature fail"
            return False
        if not self.__verify_SNN(ripMessage):
            print "1 verify SNN fail"
            return False
        self.__ack_num = ripMessage.sequence_number + 1
        #self.__Nonce1 = ripMessage.OPTIONS[0]
        self.__Nonce1 = int(ripMessage.certificate[0],16)
        return True

    def send_handshake_2(self):
        self.__seq_num = random.randint(0, 0xff)
        self.__Nonce2 = random.randint(0, 0xff)
        signed_nonce1 = self.__sign(hex(self.__Nonce1 +1)[2:])
        certs = [hex(self.__Nonce2)[2:], signed_nonce1] + self.__certs
        options = ""

        self.Write_message( True, self.__seq_num, self.__ack_num, certs, 0, True, False, True, False, "",options)

    def receive_handshake_3(self, ripMessage):
        if not self.__verify_signature(ripMessage):
            print "verify signature fail"
            return False
        if not self.__verify_ack_num(ripMessage):
            print "verify ack num fail"
            return False
        if not self.__verify_Nonce2(ripMessage):
            print "verify Nonce2 fail"
            return False
        self.__ack_num = ripMessage.sequence_number + 1
        #self.__Nonce2 = ripMessage.OPTIONS[0]
        return True

    def __handshake(self, ripMessage=""):
        if self.handshake_status == 1:
            success = self.receive_handshake_1(ripMessage)
            if not success:
                self.handshake_status = 1
                return False
            self.send_handshake_2()
            self.handshake_status = 2
        elif self.handshake_status == 2:
            success = self.receive_handshake_3(ripMessage)
            if not success:
                return False
            self.handshake_status = 3

            self.makeHigherConnection(self.__higherTransport)
        return True

    def dataReceived(self, data):
        # print "for now buffer,", self.__buffer
        # print "end buffer"
        # print "receive data,",data
        # print "end data"
        # print len(data)
        #print type(data)
        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(self.__buffer)
            self.__buffer = self.__buffer[bytesUsed:]
            #print "deserialize"
        except Exception, e:
            print "Server side: We had a deserialization error", e
            return
        #print "deserialize successful"
        if self.handshake_status != 3:
            success = self.__handshake(ripMessage)
            #print success
            if not success:
                # TODO: lose connection
                self.handshake_status = 1
                return
        else:
            #print "receieve"
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.__buffer and self.dataReceived("")

    def SendMessage(self, data):
        self.__higherTransport.write(data)



class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory